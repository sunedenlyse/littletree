﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "new tree", menuName = "Tree")]
public class ExampleTree : AbstractTree {

    public ExampleTree()
    {
        consequences = new List<AbstractTreeElement>();
    }

    protected override AbstractTreeElement InstantiateElement(string newId, AbstractTreeElement parent)
    {
        return new ExampleTreeElement(newId, (ExampleTreeElement) parent);
    }


}
