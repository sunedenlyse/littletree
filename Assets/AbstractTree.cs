﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractTree : ScriptableObject
{
    public TreeElementType consequenceListType;
    public List<AbstractTreeElement> consequences;
    public static readonly string idSeparator = "_";

    public void RemoveConsequence(string id)
    {
        // Remove the consequence with the id
        consequences.Remove(FindConsequenceWithId(id));


    }

    private AbstractTreeElement FindConsequenceWithId(string id)
    {
        foreach (AbstractTreeElement consequence in consequences)
        {
            string otherId = consequence.GetId();
            if (otherId.Equals(id))
            {
                return consequence;
            }
        }
        return null;
    }

    public void AddConsequenceAsChild(AbstractTreeElement parent)
    {
        string newId = FindNextId(parent);
        consequences.Add(InstantiateElement(newId, parent));
        SortConsequencesById();
    }

    protected virtual AbstractTreeElement InstantiateElement(string newId, AbstractTreeElement parent)
    {
        throw new NotImplementedException();
    }

    private string FindNextId(AbstractTreeElement parent)
    {
        string parentFullId;
        if (parent != null)
        {
            parentFullId = parent.GetFullId();
        }
        else
        {
            parentFullId = "";
        }
        int maxId = -1;
        List<AbstractTreeElement> childConsequences = FindConsequencesWithParentId(parentFullId);
        foreach (AbstractTreeElement childConsequence in childConsequences)
        {
            //Debug.Log("Found last digit " + lastDigit);
            if (int.Parse(childConsequence.GetId()) > maxId)
            {
                maxId = int.Parse(childConsequence.GetId());
            }
        }
        maxId++;
        return "" + maxId;

    }

    public List<AbstractTreeElement> FindConsequencesWithParentId(string parentFullId)
    {
        //Debug.Log("Trying to find consequences with  parent id " + parentId);
        List<AbstractTreeElement> resultList = new List<AbstractTreeElement>();
        foreach (AbstractTreeElement consequence in consequences)
        {
            //Debug.Log("Comparing with id" + consequence.GetId() + ", correct length: " + consequence.GetId().Length.Equals(parentId.Length + 1) + ", correct start: " + consequence.GetId().StartsWith(parentId));
            if (consequence.GetParentFullId() == parentFullId)
            {
                resultList.Add(consequence);
            }
        }
        //Debug.Log("Found " + resultList.Count + " consequences with  parent id " + parentId);
        return resultList;
    }

    public void SortConsequencesById()
    {
        consequences.Sort((p1, p2) => p1.GetFullId().CompareTo(p2.GetFullId()));
    }
}
