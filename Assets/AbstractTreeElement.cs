﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbstractTreeElement {
    private string id; 
    private string parentFullId;
    private string fullId;
    private int hierachyLevel;
    public string name;
    public TreeElementType treeElementType;
    public ConsequenceType consequenceType;//TODO When I make an editor, this field should only show up if TreeElementType == singleElement
    [Range(0,1)]
    public float weight;//TODO move to ExampleTreeElement
    public string consequenceString;

    public AbstractTreeElement(string id, AbstractTreeElement parent)
    {
        this.id = id;
        if(parent != null)
        {
            this.parentFullId = parent.fullId;
            this.fullId = parent.fullId + AbstractTree.idSeparator + id;
            this.hierachyLevel = parent.hierachyLevel + 1;
        } else
        {
            this.parentFullId = "";
            this.fullId = id;
            this.hierachyLevel = 0;
        }
        name = id;
        treeElementType = TreeElementType.SingleElement;
        consequenceType = ConsequenceType.StringConsequence;
        weight = 1f;
        consequenceString = null;
    }

    public string GetId()
    {
        return id;
    }

    public string GetFullId()
    {
        return fullId;
    }

    public string GetParentFullId()
    {
        return parentFullId;
    }

    public int GetHierachyLevel()
    {
        return hierachyLevel;
    }
}

public enum TreeElementType {OrList, AndList, SingleElement }
public enum ConsequenceType {StringConsequence}
