﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AbstractTree))]
public class ConsequenceTreeEditor : Editor {
    /*SerializedProperty property;

    private void OnEnable()
    {

        property = serializedObject.FindProperty("consequenceListType");

    }
    */

    public override void OnInspectorGUI()
    {
        //serializedObject.Update();
        //DrawDefaultInspector();
        var indent = EditorGUI.indentLevel;
        AbstractTree abstractTree = (AbstractTree)target;
        abstractTree.consequenceListType = (TreeElementType) EditorGUILayout.EnumPopup(abstractTree.consequenceListType);
        if (GUILayout.Button("Add child"))
        {
            //Debug.Log("Should add a child to tree");
            abstractTree.AddConsequenceAsChild(null);
        }
        List<AbstractTreeElement> consequences = abstractTree.consequences;
        foreach (AbstractTreeElement consequence in consequences)
        {
            EditorGUI.indentLevel = consequence.GetHierachyLevel();
            EditorGUILayout.LabelField(consequence.GetFullId());
            consequence.name = EditorGUILayout.TextField(consequence.name);
            consequence.treeElementType = (TreeElementType)EditorGUILayout.EnumPopup(consequence.treeElementType);
            consequence.weight = EditorGUILayout.FloatField(consequence.weight);
            if (!consequence.treeElementType.Equals(TreeElementType.SingleElement)){
                if (GUILayout.Button("Add child"))
                {
                    //Debug.Log("Should add a child to id" + consequence.GetId());
                    abstractTree.AddConsequenceAsChild(consequence);
                    break;
                }
            }
            if(consequence.treeElementType.Equals(TreeElementType.SingleElement))
            {
                if (GUILayout.Button("Remove"))
                {
                    abstractTree.RemoveConsequence(consequence.GetId());
                    //Debug.Log("Should remove a child to id" + consequence.GetId());
                    break;
                }
            }
        }
        EditorGUI.indentLevel = indent;
        
    }

}
