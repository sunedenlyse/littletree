﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsequenceResolver : MonoBehaviour {

    public static List<AbstractTreeElement> ResolveConsequenceTree(ExampleTree consequenceTree)
    {
        Debug.Log("resolving ConsequenceTree " + consequenceTree.name);

        List<AbstractTreeElement> resultList = new List<AbstractTreeElement>();
        float randomRoll;
        switch (consequenceTree.consequenceListType)
        {
            case TreeElementType.AndList:
                List<AbstractTreeElement> consequences = FindConsequencesAtHierarchyLevel(consequenceTree, 0);
                foreach (AbstractTreeElement consequence in consequences)
                {
                    if (consequence.weight != 1f)
                    {
                        randomRoll = UnityEngine.Random.Range(0f, 1f);
                        Debug.Log("Rolled " + randomRoll + ", compared with " + consequence.weight);
                        if (randomRoll <= consequence.weight)
                        {
                            resultList.AddRange(ResolveConsequence(consequenceTree, consequence));
                        }
                    }
                    else
                    {
                        resultList.AddRange(ResolveConsequence(consequenceTree, consequence));
                    }
                }
                break;
            case TreeElementType.OrList:
                consequences = FindConsequencesAtHierarchyLevel(consequenceTree, 0);
                float weightSum = SumConsequenceWeights(consequences);
                randomRoll = UnityEngine.Random.Range(0f, weightSum);
                Debug.Log("Rolled " + randomRoll + ", finding child consequence of Tree " + consequenceTree.name);
                resultList.AddRange(FindAndResolveChildConsequence(consequenceTree, consequences, randomRoll));
                break;
            case TreeElementType.SingleElement:
                if (consequenceTree.consequences.Count != 1)
                {
                    Debug.LogError("Found inconsistent ConsequenceTree: " + consequenceTree.name + " has " + consequenceTree.consequences.Count + " elements, but has listType singleElement");
                }
                AbstractTreeElement singleConsequence = consequenceTree.consequences[0];
                if (singleConsequence.weight != 1f)
                {
                    randomRoll = UnityEngine.Random.Range(0f, 1f);
                    Debug.Log("Rolled " + randomRoll + ", compared with " + singleConsequence.weight);
                    if (randomRoll <= singleConsequence.weight)
                    {
                        resultList.Add(singleConsequence);
                    }
                }
                else
                {
                    resultList.Add(singleConsequence);
                }
                break;
            
        }
        return resultList;
    }

    private static List<AbstractTreeElement> ResolveConsequence(ExampleTree exampleTree, AbstractTreeElement consequence)
    {
        Debug.Log("Resolving consequence " + consequence.name + " with full id " + consequence.GetFullId());
        List<AbstractTreeElement> resultList = new List<AbstractTreeElement>();
        float randomRoll;
        switch(consequence.treeElementType)
        {
            case TreeElementType.AndList:
                List<AbstractTreeElement> childConsequences = exampleTree.FindConsequencesWithParentId(consequence.GetFullId());
                foreach (AbstractTreeElement childConsequence in childConsequences)
                {
                    if (Roll01AndCompareWithWeight(childConsequence.weight))
                        {
                            resultList.AddRange(ResolveConsequence(exampleTree, childConsequence));
                        }
                    
                }
                break;
            case TreeElementType.OrList:
                childConsequences = exampleTree.FindConsequencesWithParentId(consequence.GetFullId());
                float weightSum = SumConsequenceWeights(childConsequences);
                randomRoll = UnityEngine.Random.Range(0f, weightSum);
                Debug.Log("Rolled " + randomRoll + ", finding child consequence of " + consequence.name);
                resultList.AddRange(FindAndResolveChildConsequence(exampleTree, childConsequences, randomRoll));
                break;
            case TreeElementType.SingleElement:
                resultList.Add(consequence);
                break;

        }
        return resultList;
    }

    private static List<AbstractTreeElement> FindAndResolveChildConsequence(ExampleTree exampleTree, List<AbstractTreeElement> childConsequences, float randomRoll)
    {
        float relativeRoll = randomRoll;
        foreach(AbstractTreeElement childConsequence in childConsequences)
        {
            //Debug.Log("Trying to find Consequence: Relative Roll is " + relativeRoll);
            if (relativeRoll <= childConsequence.weight)
            {
                return ResolveConsequence(exampleTree, childConsequence);
            } else
            {
                relativeRoll -= childConsequence.weight;
            }
        }
        Debug.LogError("Tried to find childconsequence with randomRoll " + randomRoll + ", but found none");
        return null;
    }

    private static float SumConsequenceWeights(List<AbstractTreeElement> consequences)
    {
        float sumOfWeights = 0f;
        foreach(AbstractTreeElement con in consequences)
        {
            sumOfWeights += con.weight;
        }
        return sumOfWeights;
    }

    private static List<AbstractTreeElement> FindConsequencesAtHierarchyLevel(ExampleTree exampleTree, int hierarchyLevel)
    {
        List<AbstractTreeElement> resultList = new List<AbstractTreeElement>();
        foreach (AbstractTreeElement consequence in exampleTree.consequences)
        {
            if (consequence.GetHierachyLevel().Equals(hierarchyLevel))
            {
                resultList.Add(consequence);
            }
        }
        //Debug.Log("Found " + resultList.Count + " consequences with id of length " + idLength);
        return resultList;
    }


    private static bool Roll01AndCompareWithWeight(float weight)
    {
        if (weight != 1f)
        {
            float randomRoll = UnityEngine.Random.Range(0f, 1f);
            Debug.Log("Rolled " + randomRoll + ", compared with " + weight);
            if (randomRoll <= weight)
                return true;
            else return false;
        }
        else return true;

    }
}
