﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

	
    public void HandleConsequenceTree(ExampleTree exampleTree)
    {
        //Debug.Log("Trying to resolve Consequences");
        List<AbstractTreeElement> treeElements = ConsequenceResolver.ResolveConsequenceTree(exampleTree);
        //Debug.Log("Found " + resultConsequences.Count + " consequences to handle");
        foreach(AbstractTreeElement treeElement in treeElements)
        {
            HandleConsequence(treeElement);
        }
    }

    private static void HandleConsequence(AbstractTreeElement treeElement)
    {
        if(!treeElement.treeElementType.Equals(TreeElementType.SingleElement))
        {
            Debug.LogError("Trying to handle consequences with list type " + treeElement.treeElementType + ". This should not happen!");
        }
        if(treeElement.consequenceType.Equals(ConsequenceType.StringConsequence))
        {
            Debug.Log(treeElement.name);
        }
    }
}
